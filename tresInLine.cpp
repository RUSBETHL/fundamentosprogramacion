#include <iostream>
#include <windows.h>
#include <stdlib.h>

using namespace std;

char tablero[][3]={{'_','_','_'},
					{'_','_','_'},
					{'_','_','_'}};
					
string jugador1,jugador2;
bool turnodeX = false;
char jugador;
int X=0,Y=0;
bool Hayganador=false;
bool NoCambiarTurno,CambiardeJugador;

/*void gotoxy(int x,int y){  
      HANDLE hcon;  
      hcon = GetStdHandle(STD_OUTPUT_HANDLE);  
      COORD dwPos;  
      dwPos.X = x;  
      dwPos.Y= y;  
      SetConsoleCursorPosition(hcon,dwPos);  
 }*/  
 
 void PintarTablero(){
 	for(int i=0; i<3; i++){
 		for(int j=0; j<3; j++){
 			cout<<tablero[i][j]<<"\t";
		 }
		 cout << endl;
	 }
 }
 bool HayEmpate(){
	for(int i = 0; i<3; i++){
 		for(int j = 0; j<3; j++){
 		if(tablero[i][j] == '_'){
		 		return false;
		 }
	 }
 }
 return true;
}
 
 void Turnos(){
 	for(int i=0; i<9; i++){
 		if(i%2==0 || NoCambiarTurno == true){
 			if(NoCambiarTurno == true){
 				turnodeX = !turnodeX;
 			}
 				jugador = turnodeX ? 'X' : 'O';
			if(CambiardeJugador == false){
				cout<<"Turno de "<<jugador2<<" con la "<<jugador<<endl;
			}
 			cout<< "Dame la posicion para la X: "<<endl;
 			cin>>X;
 			cout<< "Dame la posicion para la Y: "<<endl;
 			cin>>Y;
 			if(tablero[X][Y] != '_'){
 				cout<<"La casilla que escogiste ya esta ocupada!! Intentalo otra vez"<<endl;
 				NoCambiarTurno = true;
 				CambiardeJugador = false;
			 }else{
			 	tablero[X][Y]=jugador;
 				cout<<"El juego va de la siguiente manera: "<<endl;
 				PintarTablero();
 				NoCambiarTurno = false;
			 }
 			//HORIZONTALES
 			if(tablero[0][0]=='X' && tablero[0][1]=='X' && tablero[0][2]=='X'){
			 cout<<"El Ganador es "<<jugador2<<endl;
			 break;
		}else if(tablero[1][0]=='X' && tablero[1][1]=='X' && tablero[1][2]=='X'){
			 cout<<"El Ganador es "<<jugador2<<endl;
			 break;
		}else if(tablero[2][0]=='X' && tablero[2][1]=='X' && tablero[2][2]=='X'){
			cout<<"El Ganador es "<<jugador2<<endl;
			break;
			//VERTICALES
		}else if(tablero[0][0]=='X' && tablero[1][0]=='X' && tablero[2][0]=='X'){
			cout<<"El Ganador es "<<jugador2<<endl;
			break;
		}else if(tablero[0][1]=='X' && tablero[1][1]=='X' && tablero[2][1]=='X'){
			cout<<"El ganador es: "<<jugador2<<" Con la: "<<jugador<<endl;
			break;
		}else if(tablero[0][2]=='X' && tablero[1][2]=='X' && tablero[2][2]=='X'){
			cout<<"El ganador es: "<<jugador2<<" Con la: "<<jugador<<endl;
			break;
			//DIAGONALES
		}else if(tablero[0][0]=='X' && tablero[1][1]=='X' && tablero[2][2]=='X'){
			cout<<"El ganador es: "<<jugador2<<" Con la: "<<jugador<<endl;
			break;
		}else if(tablero[0][2]=='X' && tablero[1][1]=='X' && tablero[2][0]=='X'){
			cout<<"El ganador es: "<<jugador2<<" Con la: "<<jugador<<endl;
			break;
		}
		if(HayEmpate()){
			cout<<"Hay un empate"<<endl;
			break;
		}
		 }else{
		 	if(NoCambiarTurno == true){
		 		cout<<"Turno de "<<jugador2<<" con la "<<jugador<<endl;
			 }else{
			 	turnodeX = !turnodeX;
			 
		 	jugador = turnodeX ? 'X' : 'O';
		 	cout<<"Turno de "<<jugador1<<" Con la "<<jugador<<endl;
		 	cout<< "Dame la posicion para la X: "<<endl;
 			cin>>X;
 			cout<< "Dame la posicion para la Y: "<<endl;
 			cin>>Y;
 			if(tablero[X][Y] != '_'){
 				cout<<"La casilla que escogiste ya esta ocupada!! Intentalo otra vez"<<endl;
 				NoCambiarTurno = true;
			 }else{
			 	tablero[X][Y]=jugador;
 				cout<<"El juego va de la siguiente manera: "<<endl;
 				PintarTablero();	
 				NoCambiarTurno = false;
			 
 			//HORIZONTALES
 			if(tablero[0][0]=='O' && tablero[0][1]=='O' && tablero[0][2]=='O'){
			 cout<<"El Ganador es "<<jugador1<<" Con la: "<<jugador<<endl;
			 break;
		}else if(tablero[1][0]=='O' && tablero[1][1]=='O' && tablero[1][2]=='O'){
			 cout<<"El Ganador es "<<jugador1<<" Con la: "<<jugador<<endl;
			 break;
		}else if(tablero[2][0]=='O' && tablero[2][1]=='O' && tablero[2][2]=='O'){
			cout<<"El Ganador es "<<jugador1<<" Con la: "<<jugador<<endl;
			break;
			//VERTICALES
		}else if(tablero[0][0]=='O' && tablero[1][0]=='O' && tablero[2][0]=='O'){
			cout<<"El Ganador es "<<jugador1<<" Con la: "<<jugador<<endl;
			break;
		}else if(tablero[0][1]=='O' && tablero[1][1]=='O' && tablero[2][1]=='O'){
			cout<<"El ganador es: "<<jugador1<<" Con la: "<<jugador<<endl;
			break;
		}else if(tablero[0][2]=='O' && tablero[1][2]=='O' && tablero[2][2]=='O'){
			cout<<"El ganador es: "<<jugador1<<" Con la: "<<jugador<<endl;
			break;
			//DIAGONALES
		}else if(tablero[0][0]=='O' && tablero[1][1]=='O' && tablero[2][2]=='O'){
			cout<<"El ganador es: "<<jugador1<<" Con la: "<<jugador<<endl;
			break;
		}else if(tablero[0][2]=='O' && tablero[1][1]=='O' && tablero[2][0]=='O'){
			cout<<"El ganador es: "<<jugador1<<" Con la: "<<jugador<<endl;
			break;
		 }
	 }
			 }
		 	
}
   }
 }
int main(){
	while(true){
		cout<<"Ingresa el nombre del jugador 1: "<<endl;
 		cin>>jugador1;
 		cout<<"Ingresa el nombre del jugador 2: "<<endl;
 		cin>>jugador2;
		PintarTablero();
		cout<<""<<endl;
		Turnos();
		getchar();
		return 0;	
	}
}

